import { Router, Request, Response } from 'express';
import { FeedItem } from '../models/FeedItem';
import { requireAuth } from '../../users/routes/auth.router';
import * as AWS from '../../../../aws';
import axios from "axios";
import { config } from '../../../../config/config';

const router: Router = Router();
const imageFilterHost:string = config.dev.image_filter_host;

// Get all feed items
router.get('/', async (req: Request, res: Response) => {
    const items = await FeedItem.findAndCountAll({order: [['id', 'DESC']]});
    let promises = items.rows.map((item, index) => {
        return new Promise(async (resolve, reject) => {
            item.url = AWS.getGetSignedUrl(item.url);
            try {
                const signedRes = await axios.get(imageFilterHost+'/signed-url/'+encodeURIComponent(item.filter_image_url));
                item.filter_image_url = signedRes.data;
                items.rows[index] = item;
                resolve(item);
            } catch(e) {
                console.log("ERROR FETCHING SIGNED FILTER IMAGE");
                resolve(item);
            }
        });
    });

    Promise.all(promises).then((results) => {
        res.send(items);
    }).catch(error => {
        console.log(error);
        res.send([]);
    });
});

//@TODO
//Add an endpoint to GET a specific resource by Primary Key

// update a specific resource
router.patch('/:id', 
    requireAuth, 
    async (req: Request, res: Response) => {
        //@TODO try it yourself
        res.send(500).send("not implemented")
});


// Get a signed url to put a new item in the bucket
router.get('/signed-url/:fileName', 
    requireAuth, 
    async (req: Request, res: Response) => {
    let { fileName } = req.params;
    const url = AWS.getPutSignedUrl(fileName);
    res.status(201).send({url: url});
});

// Post meta data and the filename after a file is uploaded 
// NOTE the file name is they key name in the s3 bucket.
// body : {caption: string, fileName: string};
router.post('/', 
    requireAuth, 
    async (req: Request, res: Response) => {
    const caption = req.body.caption;
    const fileName = req.body.url;

    // check Caption is valid
    if (!caption) {
        return res.status(400).send({ message: 'Caption is required or malformed' });
    }

    // check Filename is valid
    if (!fileName) {
        return res.status(400).send({ message: 'File url is required' });
    }

    // Filter the image.
    let filterRes;
    try{
        const options = {
            headers : {'Authorization' : req.headers.authorization}
        };
        filterRes = await axios.get(imageFilterHost+'/filtered-image-from-file?image_url='+encodeURIComponent(fileName), options);
    } catch(err) {
        console.log("Error applying filters to image");
    }
     
    let signed_filter_image_url;
    if(filterRes) {
        if(filterRes.status == 200) {
            signed_filter_image_url = filterRes.data;
        }
    }
    const item = new FeedItem({
            caption: caption,
            url: fileName,
            filter_image_url: fileName+"_filtered.jpg"
    });

    const saved_item = await item.save();
    saved_item.url = AWS.getGetSignedUrl(item.url);
    saved_item.filter_image_url = signed_filter_image_url;
    
    res.status(201).send(saved_item);
});

export const FeedRouter: Router = router;